extends Node2D


# Declare member variables here.
var a = true


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_key_pressed(KEY_SPACE) and a:
		a = false
		$Websocket.send_string($Sprite.offset[0])
		$Websocket.send_string(-1)
	if not a:
		if not Input.is_key_pressed(KEY_SPACE):
			a = true
		if $Websocket.ret != "0":
			$TextEdit.text = $Websocket.ret
			$Websocket.ret = "0"
