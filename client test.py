import asyncio
import websockets

async def hello():
    uri = "ws://192.168.0.33:1777"
    async with websockets.connect(uri) as websocket:
        name = input("num ")

        await websocket.send(name)
        print(f"> {name}")
        await websocket.send("-1")

        greeting = await websocket.recv()
        print(f"< {greeting}")

asyncio.get_event_loop().run_until_complete(hello())
