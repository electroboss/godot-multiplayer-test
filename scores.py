#!/usr/bin/env python

import asyncio
import websockets

scores = [0]

async def echo(websocket, path):
    global scores
    async for message in websocket:
        print("< %s" % message)
        if int(message) == -1:
            ret = ""
            for x in sorted(scores):
                ret += "\n"+str(x)
            await websocket.send(ret)
            print("> %s" % message)
        else:
            scores.append(int(message))
            
start_server = websockets.serve(echo, "localhost", 1777)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
